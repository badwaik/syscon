# Path to your oh-my-zsh installation.
export ZSH=/home/work/.oh-my-zsh

setopt PROMPT_BANG # enables `!`

# Set name of the theme to load.
# Look in ~/.oh-my-zsh/themes/
# Optionally, if you set this to "random", it'll load a random theme each
# time that oh-my-zsh is loaded.
ZSH_THEME="honukai"

# Uncomment the following line to use case-sensitive completion.
CASE_SENSITIVE="true"

# Colors used
export TERM="xterm-256color"

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(git gnu-utils)

source $ZSH/oh-my-zsh.sh

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
export EDITOR='nvim'
# else
#   export EDITOR='mvim'
# fi

setopt inc_append_history
setopt share_history
setopt histignorespace
setopt histnostore
HISTFILE=~/.zsh_history
HISTSIZE=8192000000
SAVEHIST=8192000000
setopt autocd
unsetopt extendedglob nomatch notify
bindkey -e

autoload zkbd
source ${ZDOTDIR:-$HOME}/.zkbd/xterm.zkbd

[[ -n ${key[Insert]} ]] && bindkey "${key[Insert]}" overwrite-mode
[[ -n ${key[Home]} ]] && bindkey "${key[Home]}" beginning-of-line
[[ -n ${key[PageUp]} ]] && bindkey "${key[PageUp]}" up-line-or-history
[[ -n ${key[End]} ]] && bindkey "${key[End]}" end-of-line
[[ -n ${key[Delete]} ]] && bindkey "${key[Delete]}" delete-char
[[ -n ${key[PageDown]} ]] && bindkey "${key[PageDown]}" down-line-or-history
[[ -n ${key[Up]} ]] && bindkey "${key[Up]}" up-line-or-search
[[ -n ${key[Left]} ]] && bindkey "${key[Left]}" backward-char
[[ -n ${key[Down]} ]] && bindkey "${key[Down]}" down-line-or-search
[[ -n ${key[Right]} ]] && bindkey "${key[Right]}" forward-char
# INPUTRC Keys

alias ls='ls --color=auto -h'
# Colored Instructions

source $HOME/.profile
# Take config data from the profile
#
# Report CPU usage for commands running longer than 10 seconds
#REPORTTIME=10

source /usr/share/fzf/key-bindings.zsh
source /usr/share/fzf/completion.zsh
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

# added by travis gem
[ ! -s /home/work/.travis/travis.sh ] || source /home/work/.travis/travis.sh
